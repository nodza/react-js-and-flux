var React = require('react');
var Dropdown = require('./dropdown');

var options = {
    title: 'Choose a dessert',
    items: [
        'Donut',
        'Eclair',
        'Froyo',
        'Gingerbread',
        'Honeycomb'
    ]
};

var element = React.createElement(Dropdown, options);

React.render(element, document.querySelector('.container'));