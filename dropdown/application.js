var options = {
    thumbnailData: [{
        title: 'View Course',
        number: 24,
        imageUrl: 'https://s3.amazonaws.com/media-p.slid.es/uploads/hugojosefson/images/86267/angularjs-logo.png',
        header: 'Angular JS',
        description: 'A SUPERHEROIC JAVASCRIPT MVW FRAMEWORK'

    },
        {
            title: 'View Course',
            number: 7,
            imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/React.js_logo.svg/10' +
            '00px-React.js_logo.svg.png',
            header: 'Gulp JS',
            description: 'A TOOL TO SPEED UP YOUR DEVELOPMENT WORKFLOW'

        },
        {
            title: 'View Course',
            number: 12,
            imageUrl: 'https://avatars0.githubusercontent.com/u/6200624?v=3&s=400',
            header: 'React JS',
            description: 'A JAVASCRIPT LIBRARY FOR BUILDING USER INTERFACES'

        }]
};

// Render the class
var element = React.createElement(ThumbnailList, options);

// Place the class in the body tag
React.render(element, document.querySelector('.target'));
var Badge = React.createClass({displayName: "Badge",
    render: function() {
        return (
            React.createElement("button", {className: "btn btn-primary", type: "button"}, 
                this.props.title, " ", React.createElement("span", {className: "badge"}, this.props.number)
            )
        )
    }
});
var ThumbnailList = React.createClass({displayName: "ThumbnailList",
    render: function() {
        var list = this.props.thumbnailData.map(function(thumbnailProps) {
            return React.createElement(Thumbnail, React.__spread({},  thumbnailProps))
        });

        return React.createElement("div", null, 
            list
        )
    }
});
var Thumbnail = React.createClass({displayName: "Thumbnail",
    render: function() {
        return React.createElement("div", {className: "col-sm-6 col-md-4"}, 
            React.createElement("div", {className: "thumbnail"}, 
                React.createElement("img", {src: this.props.imageUrl}), 
                React.createElement("div", {className: "caption"}, 
                    React.createElement("h3", null, this.props.header), 
                    React.createElement("p", null, this.props.description), 
                    React.createElement("p", null, 
                        React.createElement(Badge, {title: this.props.title, number: this.props.number})
                    )
                )
            )
        )
    }
});