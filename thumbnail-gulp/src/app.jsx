var React = require('react');
var ThumbnailList = require('./thumbnail-list');

var options = {
    thumbnailData: [{
        title: 'View Course',
        number: 24,
        imageUrl: 'https://s3.amazonaws.com/media-p.slid.es/uploads/hugojosefson/images/86267/angularjs-logo.png',
        header: 'Angular JS',
        description: 'A SUPERHEROIC JAVASCRIPT MVW FRAMEWORK'

    },
        {
            title: 'View Course',
            number: 7,
            imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/React.js_logo.svg/10' +
            '00px-React.js_logo.svg.png',
            header: 'Gulp JS',
            description: 'A TOOL TO SPEED UP YOUR DEVELOPMENT WORKFLOW'

        },
        {
            title: 'View Course',
            number: 12,
            imageUrl: 'https://avatars0.githubusercontent.com/u/6200624?v=3&s=400',
            header: 'React JS',
            description: 'A JAVASCRIPT LIBRARY FOR BUILDING USER INTERFACES'

        }]
};

var element = React.createElement(ThumbnailList, options);

React.render(element, document.querySelector('.container'));